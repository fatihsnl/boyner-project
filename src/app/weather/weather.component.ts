import { Component, OnInit, Input } from '@angular/core';
import { WeatherData } from '../models/weather';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

export const DROP_ANIMATION = trigger('drop', [
  transition(':enter', [
    style({ transform: 'translateY(-200px)', opacity: 0 }),
    animate(
      '300ms cubic-bezier(1.000, 0.000, 0.000, 1.000)',
      style({ transform: 'translateY(0)', opacity: 1 })
    ),
  ]),
  transition(':leave', [
    style({ transform: 'translateY(0)', opacity: 1 }),
    animate(
      '200ms cubic-bezier(1.000, 0.000, 0.000, 1.000)',
      style({ transform: 'translateY(-200px)', opacity: 0 })
    ),
  ]),
]);

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
  animations: [
    DROP_ANIMATION
  ]
})
export class WeatherComponent implements OnInit {
  @Input() weather: WeatherData;
  constructor() { }

  ngOnInit() {
    this.weather.main.temp = Math.round(this.weather.main.temp);
  }

}
