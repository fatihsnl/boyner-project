import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather-service';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {

  constructor(private weatherService: WeatherService) { }
  public weatherData: any;
  public ngOnInit() {
    this.weatherService.getAllCityWeather('6356055,745044,2759794,2988507,3067696').subscribe(data => {
      this.weatherData = data;
    });
  }
}
