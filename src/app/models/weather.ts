export interface WeatherList {
  list: WeatherData[];
}
export interface WeatherDetailList {
  list: WeatherData[];
  city: City;
}
export class WeatherData {
  name: string;
  weather: Weather[];
  main: Main;
  wind: Wind;
}
class Weather {
  description: string;
}

class Main {
  temp: number;
}

class Wind {
  speed: string;
  deg: string;
}

class City {
  name: string;
}
