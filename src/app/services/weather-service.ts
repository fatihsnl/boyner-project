import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WeatherList, WeatherDetailList } from '../models/weather';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class WeatherService {
  public appId = '027589c536b3e8ba51f4b85fe4e037f4';

  constructor(private http: HttpClient) { }

  public getAllCityWeather(ids: string) {
    const url = 'http://api.openweathermap.org/data/2.5/group?' +
      `id=${ids}&units=metric&appid=${this.appId}`;
    return this.http.get<WeatherList>(url).map(data => data.list);
  }
  public getForecastForCity(id: string): Observable<WeatherDetailList> {
    const url = 'http://api.openweathermap.org/data/2.5/forecast?' +
      `id=${id}&units=metric&appid=${this.appId}`;
    return this.http.get<WeatherDetailList>(url);
  }
}
