import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { WeatherService } from './services/weather-service';
import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherComponent } from './weather/weather.component';
import { Routes, RouterModule } from '@angular/router';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { RoundPipe } from './round.pipe';

export const ROUTES: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'weather-list' },
  {
    path: 'weather-list',
    component: WeatherListComponent,
    data: { animation: 'list' }
  },
  {
    path: 'weather-detail/:Id/:Name',
    component: WeatherDetailComponent,
    data: { animation: 'detail' }
  },
];
@NgModule({
  declarations: [
    AppComponent,
    WeatherListComponent,
    WeatherComponent,
    WeatherDetailComponent,
    RoundPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
