import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.css']
})
export class WeatherDetailComponent implements OnInit {
  public weatherDetail: any;
  public cityName: string;

  constructor(private weatherService: WeatherService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.cityName = this.route.snapshot.params['Name'];
    this.weatherService.getForecastForCity(this.route.snapshot.params['Id']).subscribe(data => {
      this.weatherDetail = data.list;
    });
  }

}
